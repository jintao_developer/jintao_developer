$(function () {
    $.get("header-right.html", function (result) {
        $(".header-right").append(result);
        registerMenuClick();

    });
    $.get("footer-bg.html", function (result) {
        $(".footer-bg").append(result);
    });
    indexLeft('');

    function indexLeft(link) {
        let links = link == '' ? "index-header-left.html" : link;
        $.get(links, function (result) {
            $(".header-left")[0].innerHTML = result;
            if (link.indexOf("index") != -1 || link == '') {
                initIndexImg();
                indexInitJs();
            }
            if (link.indexOf("catalogue") != -1) catalogueInitJs();
            if (link.indexOf("distributors") != -1) distributorsMouseEvent();
        });
    }

    function registerMenuClick() {
        $(".nav li").click(function () {
            $(this).siblings().removeAttr("class");
            $(this).attr("class", "active");
            let link = $(this).find("a").attr("link");
            indexLeft(link);

        });
    }

    $.get("ftr-bg.html", function (result) {
        $("body").append(result);
    });

    //index js new display none
    function indexInitJs() {
        $(".lb-album li a").click(function () {
            $(".float_div_new").css("display", "none");
        });
        $(".lb-close").click(function () {
            $(".float_div_new[displayFlag='ok']").css("display", "block")
        });
    }

    //init index img
    function initIndexImg(result) {
        let template = $("#indexTemplate");
        let tempResult = template.find(".lb-album");
        let tempLi = $(tempResult.find("li")[0].outerHTML);
        // let tempResultArray = []
        tempLi.css("display", "block");
        for (let i = 28; i > 0; i--) {
            let name = "35gm00" + (i < 10 ? ('0' + i) : i);
            let imgUrl = "images/catalogue/" + name + ".jpg";
            let fullImgUrl = "images/full/" + i + ".jpg";
            let imgId = "index-image-" + i;
            tempLi.find("a img").attr("src", imgUrl);
            tempLi.find(".lb-overlay img").attr("src", fullImgUrl);
            tempLi.find(">a").attr("href", "#" + imgId);
            tempLi.find(".lb-overlay").attr("id", imgId);
            if (i == 3 || i == 4 || i == 14 || i == 23 || i == 24) {
                tempLi.find(".float_div_new").css("display", "block")
                tempLi.find(".float_div_new").attr("displayFlag", "ok");
            }
            tempResult.append(tempLi[0].outerHTML);
            // tempResultArray.push(tempLi[0].outerHTML)
            tempLi.find(".float_div_new").css("display", "none");
            tempLi.find(".float_div_new").removeAttr("displayFlag")
            if (i == 13) i = 6;
            if (i == 22) i = 17;
        }
        // tempResult.append(tempResultArray[0] + tempResultArray.slice(2, 5).join("") + tempResultArray[1] + tempResultArray.slice(5).join(""))
        return template.html();
    }

    //catalogue script init
    function catalogueInitJs() {
        for (let i = 25; i < 38; i++) {
            let template = $("#catalogueTemplate");
            let name = "35gm00" + (i < 10 ? ('0' + i) : i);
            let multipleIndex = (i - 24);
            if (i >= 25) {
                name = "35gm-multiple-" + multipleIndex;
            }
            let imgUrl = "images/catalogue/" + name + ".jpg";
            template.find(".img a").attr("link", "images/full/" + i + ".jpg");
            if (i >= 25) {
                template.find(".img a").attr("link", "images/full/35gm-multiple-" + multipleIndex + ".jpg");
            }
            template.find("img").attr("src", imgUrl);
            template.find(".desc").html(name);
            $(".content").append(template[0].innerHTML);
            if (i == 6) i = 12;
            if (i == 16) i = 21;
            if (i == 22) i = 23;
        }
        PAGE_STACK.push($(".header-left")[0].innerHTML);
        CATALOGUE_CACHE = $(".header-left")[0].innerHTML;
    }

    //catalogue product features link
    function catalogueProductFeatures(t) {
        let link = "catalogue-product-features/product-cover-header-left.html";
        let imgUrl = $(t).attr("link");
        $.get(link, function (result) {
            let res = $(result).find("img").attr("src", imgUrl);
            $(".header-left")[0].innerHTML = res[0].outerHTML;
        });
    }

    //distributors mouse event
    function distributorsMouseEvent() {
        $(".mp").mouseover(function () {
            $(this).find(".feng").show();
        }).mouseout(function () {
            $(this).find(".feng").hide();
        });
    }

});

//Global function

//catalogue product features link
function catalogueProductCover(t, identifier) {
    let link = "catalogue-product-features/product-cover-header-left.html";
    let imgUrl = $(t).attr("link");
    identifier = identifier == undefined ? $(t).siblings(".desc").text() : identifier;
    $.get(link, function (result) {
        let res = $(result);
        res.find("img").attr("src", imgUrl);
        res.find(".readmore a").attr("identifier", identifier);
        let trackLink = $(res.find(".readmore")[0].outerHTML);
        $(".header-left")[0].innerHTML = res[0].outerHTML;
        PAGE_STACK.push(res[0].outerHTML);
    });
}

function productReview(t, identifier) {
    identifier = identifier == undefined ? $(t).attr("identifier") : identifier;
    if (productReviewMap[identifier] > 0) {
        let link = 'track-link-header-left.html';
        $.get(link, function (result) {
            $(".header-left")[0].innerHTML = result;
            PAGE_STACK.push(result);
        });
    } else {
        alert("No Track-Link Contents");
    }
}

//product review click event
function productList(t, feature) {
    let identifier = $(t).attr("identifier");
    let count = productSampleMap[identifier.toLowerCase()];
    if (feature != undefined) count = productFeaturesMap[identifier.toLowerCase()];
    if (count == undefined || count <= 0) {
        alert("No Pictures")
        return;
    }
    let preFix = "product-sample";
    if (feature != undefined) preFix = "product-features";
    let link = "catalogue-product-features/" + preFix + "-header-left.html";
    $.get(link, function (result) {
        let template = $(result);
        let listDiv = $(template.find(".responsive")[0].outerHTML);
        let hideListDiv = $(template.find(".lb-overlay")[0].outerHTML);
        for (let i = 1; i <= count; i++) {
            let imgUrl = "images/" + preFix + "/" + identifier + "/" + i + ".jpg";
            let imgId = "image-" + identifier + "-" + i;
            if (i == 1) {
                template.find("img").attr("src", imgUrl);
                template.find(".img a").attr("href", "#" + imgId);
                template.find(".lb-overlay").attr("id", imgId);
            } else {
                listDiv.find("img").attr("src", imgUrl);
                listDiv.find(".img a").attr("href", "#" + imgId);
                hideListDiv.attr("id", imgId);
                hideListDiv.find("img").attr("src", imgUrl);
                template.find(".content").append(listDiv[0].outerHTML);
                if (identifier == '35gm0003') template.find(".desc").text("David Ng - Hong Hong.");
                template.append(hideListDiv[0].outerHTML);
            }
        }
        $(".header-left")[0].innerHTML = template[0].outerHTML;
        PAGE_STACK.push(template[0].outerHTML);
    });
}

function backPage(i) {
    PAGE_STACK.pop();
    let tempHistory = PAGE_STACK[PAGE_STACK.length - 1];
    if (tempHistory == undefined) tempHistory = CATALOGUE_CACHE;
    if (i == 1) $(".header-left")[0].innerHTML = tempHistory;
    if (i == 0) $(".header-left")[0].innerHTML = PAGE_STACK[1];
}

//news click link
function newsClickEvent(t, index, count) {
    let title = $(t).text();
    $.get("news-header-left.html", function (result) {
        $(window).scrollTop(0);
        let template = $(result);
        //deep clone
        let listDiv = $(template.find(".grid")[0].outerHTML);
        listDiv.find("h4").remove();
        for (let i = 1; i <= count; i++) {
            let imgUrl = "images/news/00" + index + "/" + i + ".jpg";
            if (i == 1) {
                template.find(".blog-para img").attr("src", imgUrl);
                template.find(".grid h4 a").text(title);
            } else {
                listDiv.find(".blog-para img").attr("src", imgUrl);
                template.find(".blog").append(listDiv[0].outerHTML);
            }
        }
        $(".header-left")[0].innerHTML = template[0].outerHTML;
    });

}